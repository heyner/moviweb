<?php
include('dist/libs/conexion.php');

$url_principal  = '';
$imagen_principal = '';
$titulo_principal = '';
$descripcion_corta = '';
$id_principal = 0;

$articulos = $db
  ->where('estado_en', 3)
  ->orderBy('publicado_en', 'DESC')
  ->objectBuilder()->get('articulos', 1);

if ($db->count > 0) {
  $id_principal = $articulos[0]->Id;
  $url_principal  = $articulos[0]->url_en;
  $imagen_principal = $articulos[0]->imagen;
  $titulo_principal = $articulos[0]->titulo_en;
  $descripcion_corta = substr($articulos[0]->meta_en, 0, 150);
}

$ls_articulos = '';

$articulos = $db
  ->where('estado_en', 3)
  ->where('Id', $id_principal, '!=')
  ->orderBy('publicado_en', 'DESC')
  ->objectBuilder()->get('articulos', 4);

foreach ($articulos as $articulo) {
  $ls_articulos .= '<div class="ContentBlogPrincipal__box">
                        <div class="ContentBlogPrincipal__image">
                          <img src="dist/assets/images/' . $articulo->imagen . '" alt="Imagen de Blog">
                        </div>
                        <div class="ContentBlogPrincipal__text">
                          <div class="ContentBlogPrincipal__text-titulo">
                            <span>' . $articulo->titulo_en . '</span>
                          </div>
                          <div class="ContentBlogPrincipal__text-parrafo">
                            <span>' . substr($articulo->meta_en, 0, 200) . '</span>
                          </div>
                          <div class="ContentBlogPrincipal__text-btn">
                            <div class="Conten-slide-btn">
                              <a href="article/' . $articulo->url_en . '">
                                <div class="Btn-slide">
                                  <span class="Btn-amaslide">Learn More</span>
                                </div>
                              </a>
                            </div>
                          </div>
                          <div class="ContentBlogPrincipal__text-redes">
                            <ul>
                              <li><a href="https://www.facebook.com/movicoms/" target="_blank"><span class="icon-facebook2"></span></a></li>
                              <li><a href="https://www.linkedin.com/company/movicoms/" target="_blank"><span class="icon-linkedin"></span></a></li>
                            </ul>
                          </div>
                        </div>
                    </div>';
}

$ls_casos = '';

$casos = $db
  ->orderBy('RAND()')
  ->objectBuilder()->get('casos_estudio_en', 4);

foreach ($casos as $caso) {
  $ls_casos .= '<a href="case-study/' . $caso->url_cs . '">
                    <div class="Contencasesstudy__int">
                      <div class="Contencasesstudy__title">
                        <span>' . $caso->titulo_cs . '</span>
                        <span class="Learnbtn">Learn More</span>
                      </div>
                      <div class="Contencasesstudy__img">
                        <div class="Contencasesstudy__img-int">
                          <img src="dist/assets/images/' . $caso->imagen_cs . '" alt="">
                        </div>
                      </div>
                    </div>
                  </a>';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="In a world where everything is constantly changing, our main focus is to impact our target audience in an organic manner that drives our clients’ main objective.">
  <title>Movi Communications</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/swiper-bundle.min.css" />
  <style>
    .swiper-button-next,
    .swiper-button-prev {
      position: relative;
      display: inline-block;
      margin-right: 40px;
      height: 10px;
      outline: none;
    }

    .swiper-button-next:after,
    .swiper-button-prev:after {
      content: '';
    }

    .swiper-pagination {
      width: 77%;
      height: 4px;
      left: 0;
      top: 0;
      position: relative;
      display: inline-block;
    }

    #background-video {
      width: 100%;
      height: 700px;
      object-fit: cover;
    }

    .Conten-superior-btn {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 1;
    }

    .Isize {
      width: 310px;
      height: 500px;
    }

    @media screen and (max-width: 1024px) {
      #background-video {
        height: 500px;
      }

      .swiper-container {
        height: 350px !important;
      }

      .Conten-superior-btn {
        left: 43%;
      }
    }
  </style>
</head>

<body>

  <header>
    <?php include("dist/libs/secc_include/secc-header.php") ?>
  </header>

  <a href="article/<?php echo $url_principal ?>">
    <section class="Relative" style="margin-bottom: 100px;">
      <div class="Headerprincipalblog" style="background-image: url(dist/assets/images/<?php echo $imagen_principal ?>)">

      </div>
      <div class="Headerprincipalblog__label">
        <div class="Headerprincipalblog__label-text">
          <h2><?php echo $titulo_principal ?></h2>
          <span><?php echo $descripcion_corta ?></span>
        </div>
      </div>
    </section>
  </a>

  <section>
    <div class="ContentBlogPrincipal">
      <div class="ContentBlogPrincipal__sec">
        <?php echo $ls_articulos ?>
      </div>
      <div class="ContentBlogPrincipal__sec">
        <span class="Blogtext_strong">CASE STUDY</span>
        <div class="Contencasesstudy">
          <?php echo $ls_casos ?>
        </div>
      </div>
    </div>
  </section>

  <div class="Modalboxcontent Oculto">
    <div class="Modalboxcontent__int">
      <div class="Modalboxcontent__context">
        <div class="Modalboxcontent__context-int">
          <div class="Modalboxcontent__context-top">
            <div class="Modalboxcontent__context-top-left">
            </div>
            <div class="Modalboxcontent__context-top-right">
              <a href="javascript:void(0)" role="button" class="Modal-btn-cerrar"><i class="icon-cross"></i></a>
            </div>
          </div>
          <div class="Modalboxcontent__context-middle">
            <div class="Modalboxcontent__iframe">
              <iframe id="Moviframe" frameborder="0" allowfullscreen="" allow="autoplay; fullscreen" src=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include("dist/libs/secc_include/secc-contacto.php") ?>

  <footer>
    <?php include("dist/libs/secc_include/secc-footer.php") ?>
  </footer>
</body>
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/menu-scroll.js"></script>
<script src="dist/js/menu-public.js?<?php echo time()  ?>"></script>
<script src="dist/js/smooth-scroll.polyfills.js"></script>
<script src="dist/js/funcion-scroll.js"></script>
<script src="dist/js/swiper-bundle.min.js"></script>
<script src="dist/js/aos.js"></script>
<script>
  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 50,
    slidesPerGroup: 1,
    // centeredSlides: true,
    centerInsufficientSlides: true,
    loop: false,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    }
  });
</script>

<script>
  AOS.init();
</script>

</html>
