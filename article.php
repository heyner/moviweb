<?php
include('dist/libs/conexion.php');
$ls_articulos = '';

if (isset($_REQUEST['art'])) {
  $articulos = $db
    ->where('url_en', $_REQUEST['art'])
    ->objectBuilder()->get('articulos');

  if ($db->count > 0) {
    $titulo = $articulos[0]->titulo_en;
    $imagen = $articulos[0]->imagen;
    $descripcion = $articulos[0]->descripcion_en;
    $autor = '';
    $autor_imagen = 'https://mighty.tools/mockmind-api/content/human/44.jpg';

    $users = $db
      ->where('Id', $articulos[0]->autor)
      ->objectBuilder()->get('usuarios', null, 'Id, CONCAT(nombre," ", apellido) AS nombre, imagen');

    if ($db->count > 0) {
      $autor = $users[0]->nombre;
      if ($users[0]->imagen != '') {
        $autor_imagen = 'dist/assets/images/' . $users[0]->imagen;
      }
    }

    $articulos = $db
      ->where('estado_en', 3)
      ->where('Id', $articulos[0]->Id, '!=')
      ->orderBy('publicado_en', 'DESC')
      ->objectBuilder()->get('articulos', 3);

    foreach ($articulos as $articulo) {
      $ls_articulos .= '<a href="article/' . $articulo->url_en . '">
                          <div class="Contentblog__box">
                            <div class="Contentblog__box-izq">
                              <div class="Contentblog__box-text">
                                <span>' . $articulo->titulo_en . '</span>
                              </div>
                            </div>
                            <div class="Contentblog__box-der">
                              <div class="Contentblog__box-image">
                                <img src="dist/assets/images/' . $articulo->imagen . '" alt="">
                              </div>
                            </div>
                          </div>
                        </a>';
    }
  } else {
    header('Location: ../index');
  }
} else {
  header('Location: ../index');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="In a world where everything is constantly changing, our main focus is to impact our target audience in an organic manner that drives our clients’ main objective.">
  <title>Movi Communications</title>
  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/swiper-bundle.min.css" />
  <style>
    .swiper-button-next,
    .swiper-button-prev {
      position: relative;
      display: inline-block;
      margin-right: 40px;
      height: 10px;
      outline: none;
    }

    .swiper-button-next:after,
    .swiper-button-prev:after {
      content: '';
    }

    .swiper-pagination {
      width: 77%;
      height: 4px;
      left: 0;
      top: 0;
      position: relative;
      display: inline-block;
    }

    #background-video {
      width: 100%;
      height: 700px;
      object-fit: cover;
    }

    .Conten-superior-btn {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 1;
    }

    .Isize {
      width: 310px;
      height: 500px;
    }

    @media screen and (max-width: 1024px) {
      #background-video {
        height: 500px;
      }

      .swiper-container {
        height: 350px !important;
      }

      .Conten-superior-btn {
        left: 43%;
      }
    }
  </style>
</head>

<body>

  <header>
    <?php include("dist/libs/secc_include/secc-header.php") ?>
  </header>

  <section>
    <div class="Headerblog" style="background-image: url(dist/assets/images/<?php echo $imagen ?>)">
      <div class="Headerblog__int">
        <div class="Headerblog__text">
          <h1><?php echo $titulo ?></h1>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="Contentblog">
      <div class="Contentblog__int">
        <div class="Contentblog__autor">
          <div class="Contentblog__image">
            <img src="<?php echo $autor_imagen ?>" alt="">
          </div>
          <div class="Contentblog__label">
            <strong>Por: </strong> <?php echo $autor ?>
          </div>
        </div>
      </div>
      <div class="Contentblog__int">
        <?php echo $descripcion ?>
      </div>

      <div class="Contentblog__interesar">
        <div class="Contentblog__interesar-text">
          <span>YOU MIGHT BE INTERESTED</span>
        </div>
        <div class="Contentblog__recomendados">
          <?php echo $ls_articulos ?>
        </div>
      </div>
    </div>
  </section>

  <div class="Modalboxcontent Oculto">
    <div class="Modalboxcontent__int">
      <div class="Modalboxcontent__context">
        <div class="Modalboxcontent__context-int">
          <div class="Modalboxcontent__context-top">
            <div class="Modalboxcontent__context-top-left">
            </div>
            <div class="Modalboxcontent__context-top-right">
              <a href="javascript:void(0)" role="button" class="Modal-btn-cerrar"><i class="icon-cross"></i></a>
            </div>
          </div>
          <div class="Modalboxcontent__context-middle">
            <div class="Modalboxcontent__iframe">
              <iframe id="Moviframe" frameborder="0" allowfullscreen="" allow="autoplay; fullscreen" src=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </div>
  <?php include("dist/libs/secc_include/secc-contacto.php") ?>

  <footer>
    <?php include("dist/libs/secc_include/secc-footer.php") ?>
  </footer>
</body>
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/menu-scroll.js"></script>
<script src="dist/js/menu-public.js?<?php echo time()  ?>"></script>
<script src="dist/js/smooth-scroll.polyfills.js"></script>
<script src="dist/js/funcion-scroll.js"></script>
<script src="dist/js/swiper-bundle.min.js"></script>
<script src="dist/js/aos.js"></script>
<script>
  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 50,
    slidesPerGroup: 1,
    // centeredSlides: true,
    centerInsufficientSlides: true,
    loop: false,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    }
  });
</script>

<script>
  AOS.init();
</script>

</html>
