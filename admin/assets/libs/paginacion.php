<?php

/**
 *
 */
class paginacion
{
	function __construct($parametros = array())
	{
		if (count($parametros) > 0)
			$this->iniciar($parametros);
	}

	function iniciar($parametros = array())
	{
		foreach ($parametros as $key => $val) {
			$this->$key = $val;
		}
	}

	function crearlinks()
	{
		$paginacion = '';
		$siguiente = $this->pagina + 1;
		$anterior = $this->pagina - 1;
		$penultima = $this->ultima_pag - 1;

		$paginacion = '';

		if ($this->pagina > 1) {
			$paginacion .= '<li class="page-item"><a class="page-link mpag" id="1" href="#" aria-label="Previous"><span aria-hidden="true">«</span></a></li>';
		} else {
			$paginacion .= '<li class="page-item disabled"><a class="page-link" href="#"><span aria-hidden="true">«</span></a></li>';
		}

		if ($this->ultima_pag < 7 + ($this->adyacentes * 2)) {
			for ($contador = 1; $contador <= $this->ultima_pag; $contador++) {
				if ($contador == $this->pagina) {
					$paginacion .= '<li class="page-item active"><a class="page-link" href="#">' . $contador . '</a></li>';
				} else {
					$paginacion .= '<li class="page-item"><a class="page-link mpag" href="#" id="' . ($contador) . '">' . $contador . '</a></li>';
				}
			}
		} elseif ($this->ultima_pag > 5 + ($this->adyacentes * 2)) {
			if ($this->pagina < 1 + ($this->adyacentes * 2)) {
				for ($contador = 1; $contador < 2 + ($this->adyacentes * 2); $contador++) {
					if ($contador == $this->pagina) {
						$paginacion .= '<li class="page-item active"><a class="page-link" href="#">' . $contador . '</a></li>';
					} else {
						$paginacion .= '<li class="page-item"><a class="page-link mpag" href="#" id="' . ($contador) . '">' . $contador . '</a></li>';
					}
				}
			} elseif ($this->ultima_pag - ($this->adyacentes * 2) > $this->pagina && $this->pagina > ($this->adyacentes * 2)) {
				for ($contador = $this->pagina - $this->adyacentes; $contador <= $this->pagina + $this->adyacentes; $contador++) {
					if ($contador == $this->pagina) {
						$paginacion .= '<li class="page-item active"><a class="page-link" href="#">' . $contador . '</a></li>';
					} else {
						$paginacion .= '<li class="page-item"><a class="page-link mpag" href="#" id="' . ($contador) . '">' . $contador . '</a></li>';
					}
				}
			} else {
				for ($contador = $this->ultima_pag - (1 + ($this->adyacentes * 2)); $contador <= $this->ultima_pag; $contador++) {
					if ($contador == $this->pagina) {
						$paginacion .= '<li class="page-item active"><a class="page-link" href="#">' . $contador . '</a></li>';
					} else {
						$paginacion .= '<li class="page-item"><a class="page-link mpag" href="#" id="' . ($contador) . '">' . $contador . '</a></li>';
					}
				}
			}
		}

		if ($this->pagina < $this->ultima_pag) {
			$paginacion .= '<li class="page-item"><a class="page-link mpag" id="' . ($this->ultima_pag) . '" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>';
		} else {
			$paginacion .= '<li class="page-item disabled"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true">»</span></a></li>';
		}

		return $paginacion;
	}
}
