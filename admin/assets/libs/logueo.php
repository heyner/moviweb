<?php
require_once 'conexion.php';

$data = $_REQUEST['login'];
$msg = [];

$sel = $db
    ->where('correo', $data['email'])
    ->where('estado', 1)
    ->objectBuilder()->get('usuarios');

if ($db->count > 0) {
    $decrypted = $sel[0]->contrasena;
    $hash = password_verify($data['password'], $decrypted);

    if ($hash) {
        $db->where('Id', $sel[0]->Id)
            ->update('usuarios', ['ingreso' => date('Y-m-d H:i:s')]);


        session_start();
        $_SESSION['User_Movicoms'] = $sel[0]->Id;
        $_SESSION['Tipo_Movicoms'] = $sel[0]->tipo;

        $msg['redirect'] = 'home';

        $msg['status'] = true;
    } else {
        $msg['status'] = false;
    }
} else {
    $msg['status'] = false;
}

echo json_encode($msg);
