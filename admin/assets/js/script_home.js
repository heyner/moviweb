$(document).ready(function () {
    data = new FormData();
    var articuloId = 0;

    listar_articulos(1);

    tinymce.init({
        selector: 'textarea',
        language: 'es_MX',
        browser_spellcheck: true,
        contextmenu: false,
        height: 400,
        menubar: false,
        plugins: [
            'advlist', 'autolink', 'lists', 'link', 'image', 'charmap', 'preview',
            'anchor', 'searchreplace', 'visualblocks', 'fullscreen',
            'insertdatetime', 'media', 'table', 'code', 'wordcount'
        ],
        toolbar: 'undo redo | blocks | bold italic backcolor | ' +
            'alignleft aligncenter alignright alignjustify | ' +
            'bullist numlist outdent indent | removeformat',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

    $('.Add-article').on('click', function () {
        $('#Form-articulo')[0].reset();
        $('.autor-articulo').val('');
        $('[name="articulo[autor]"]').val('');
        $('.temp-destacada').remove();
    })

    $('.titulo-en').on('keyup', function () {
        var url = $(this).val().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^a-z0-9\s]/g, "").replace(/ /g, '-').replace(/ñ/g, 'n');

        $('.url-en').val(url);
    });

    $('.titulo-es').on('keyup', function () {
        var url = $(this).val().toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[^a-z0-9\s]/g, "").replace(/ /g, '-').replace(/ñ/g, 'n');

        $('.url-es').val(url);
    });

    var $uploadCrop,
        tempFilename,
        rawImg;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                rawImg = e.target.result;
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            nmensaje('error', 'Error', 'Su navegador no soporta FileReader API');
        }
    }

    $('.img-destacada').on('click', function () {
        $('#destacada').click();
    });

    $('#destacada').on('change', function () {
        if ($(this).val() !== '') {
            if (/^image\/\w+/.test(this.files[0].type)) {
                tempFilename = $(this).val().replace(/C:\\fakepath\\/i, '');
                tempFilename = tempFilename.substr(0, tempFilename.lastIndexOf('.')) || tempFilename;
                readFile(this);
                modal({
                    type: 'info',
                    title: 'Crop Image',
                    text: '<div class="modal-body">' +
                        '<div id="upload-demo" class="center-block"></div>' +
                        '</div>',
                    size: 'large',
                    callback: function (result) {
                        if (result === true) {
                            $uploadCrop.croppie('result', {
                                type: 'base64',
                                format: 'jpeg',
                                size: 'original',
                                // size: {
                                // 	width: 800,
                                // 	height: 500
                                // },
                            }).then(function (resp) {
                                data.append('articulo[img-producto]', resp);
                                data.append('articulo[img-producto-nombre]', tempFilename);
                                $('.temp-destacada').remove();
                                $('#destacada').after('<div class="Pictures-des-mos temp-destacada"><br>' +
                                    ' <div class="Pictures-des-mos-int">' +
                                    '<img src="' + resp + '">' +
                                    '</div>' +
                                    '<div class="Pictures-des-mos-name-image">' +
                                    '<p>' + tempFilename + '</p>' +
                                    '</div>' +
                                    '<div class="Pictures-des-mos-edit Left">' +
                                    '<a href="javascript://" class="img-eliminar">Delete</a>' +
                                    '</div>' +
                                    '</div>');
                            });
                        }
                    },
                    onShow: function (result) {
                        $uploadCrop = $('#upload-demo').croppie({
                            viewport: {
                                width: 1200,
                                height: 600,
                            },
                            enforceBoundary: false,
                            enableExif: true,
                            enableResize: true
                        });
                        $uploadCrop.croppie('bind', {
                            url: rawImg
                        }).then(function () {
                            console.log('jQuery bind complete');
                        });
                    },
                    closeClick: false,
                    animate: true,
                    buttonText: {
                        yes: 'Confirmar',
                        cancel: 'Cancelar'
                    }
                });
            } else {
                // nmensaje('error', 'Error', 'No es un formato válido');
                $(this).val('');
            }
        }
    });

    $('body').on('click', '.img-eliminar', function () {
        data.append('eliminar-imagen', 1);
        $('.temp-destacada').remove();
    });

    $('.autor-articulo').on('click, focus', function () {
        $(this).autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: 'assets/libs/acc_articulos',
                    type: 'POST',
                    data: {
                        'articulo[opc]': 'Buscar-autor',
                        'articulo[buscar]': request.term
                    },
                    dataType: 'json',
                    success: function (data) {
                        if (data.status == false) {
                            result = [{
                                label: 'The author was not found',
                                value: response.term
                            }];

                            $('[name="articulo[autor]"]').val('');
                            response(result);
                        } else {
                            response($.map(data.items, function (item) {
                                return {
                                    label: item.nombre,
                                    value: item.nombre,
                                    Id: item.Id,
                                }
                            }))
                        }
                    }
                })
            },
            autoFocus: true,
            minLength: 2,
            delay: 20,
            select: function (event, ui) {
                if (ui.item.label != 'The author was not found') {
                    // inputNombre.val(ui.item.label);
                    $('[name="articulo[autor]"]').val(ui.item.Id);
                } else {
                    event.preventDefault();
                    $('[name="articulo[autor]"]').val('');
                }
            }
        });
    });

    $('.autor-articulo').on('keyup', function () {
        if ($(this).val().trim() == '') {
            $('[name="articulo[autor]"]').val('');
        }
    });

    $('#Form-articulo').on('submit', function (e) {
        e.preventDefault();
        $('[type=submit]', this).prop('disabled', true);

        if (!this.checkValidity()) {
            e.stopPropagation()
            this.classList.add('was-validated')
        } else {
            $('.load-form').show();
            otradata = $(this).serializeArray();

            $.each(otradata, function (key, input) {
                data.append(input.name, input.value);
            });

            data.append('articulo[opc]', 'nuevo-articulo');
            data.append('articulo[id]', articuloId);

            $.ajax({
                url: 'assets/libs/acc_articulos',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    $('.load-form').hide();
                    $('#destacada').val('');
                    if (data.status == true && data.error_imagen == false) {
                        articuloId = 0;
                        $('#Modal-articulo').modal('hide');
                        $('#Form-articulo')[0].reset();
                        listar_articulos(1);
                    } else if (data.status == true && data.error_imagen == true) {
                        // nmensaje('error', 'Error', 'La imagen principal no pudo ser subida');
                    } else if (data.status == false) {
                        // $('.progress-bar', '#progreso_destacada').fadeOut(500, function () {
                        //     $('#progreso_destacada').empty();
                        // });
                        // nmensaje('error', 'Error', data.motivo);
                    }
                }
            });
        }

        $('[type=submit]', this).prop('disabled', false);
    });

    function listar_articulos(pagina) {
        $('.load-listado-articulos').show();
        $('#listado-articulos').empty();
        $.post('assets/libs/acc_articulos', {
            'articulo[nombre]': $('.Buscar-nombre').val(),
            'articulo[fecha-creacion]': $('.Buscar-fecha-creacion').val(),
            'articulo[fecha-publicacion]': $('.Buscar-fecha-publicacion').val(),
            'articulo[estado]': $('.Buscar-estado').val(),
            'articulo[pagina]': pagina,
            'articulo[opc]': 'listado-articulos'
        }, function (data) {
            $('.load-listado-articulos').hide();
            $('#listado-articulos').html(data.listado);
            $('.Tabla-articulos .pagination').html(data.paginacion);
        }, 'json');
    }

    $('body').on('click', '.Tabla-articulos .mpag', function () {
        listar_articulos($(this).prop('id'));
    });

    $('.Buscar-articulos').on('click', function () {
        listar_articulos(1);
    });

    $('body').on('click', '.Editar-articulo', function () {
        articuloId = $(this).attr('data-articulo');
        $('.temp-destacada').remove();

        $.post('assets/libs/acc_articulos', { 'articulo[id]': articuloId, 'articulo[opc]': 'datos-articulo' }, function (data) {
            $('#Form-articulo')[0].reset();
            $('#Form-articulo').find('[name="articulo[titulo_es]"]').val(data.datos.titulo_es);
            $('#Form-articulo').find('[name="articulo[titulo_en]"]').val(data.datos.titulo_en);
            $('#Form-articulo').find('[name="articulo[url_es]"]').val(data.datos.url_es);
            $('#Form-articulo').find('[name="articulo[url_en]"]').val(data.datos.url_en);
            tinymce.get('articulo-meta-descripcion-es').setContent(data.datos.meta_es);
            tinymce.get('articulo-meta-descripcion-en').setContent(data.datos.meta_en);
            tinymce.get('articulo-descripcion-es').setContent(data.datos.descripcion_es);
            tinymce.get('articulo-descripcion-en').setContent(data.datos.descripcion_en);
            $('#Form-articulo').find('[name="articulo[estado_es]"]').val(data.datos.estado_es);
            $('#Form-articulo').find('[name="articulo[estado_en]"]').val(data.datos.estado_en);
            $('#Form-articulo').find('.autor-articulo').val(data.datos.autor);
            $('#Form-articulo').find('[name="articulo[autor]"]').val(data.datos.autor_id);
            $('#Modal-articulo').modal('show');
        }, 'json');
    })

});
