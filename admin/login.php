<?php
session_start();

if (isset($_SESSION['User_Movicoms'])) {
  header('Location: home');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="../admin/assets/css/bootstrap-icons.css">
  <link rel="stylesheet" href="../admin/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="../admin/assets/css/stylescssadmin.css?v<?php echo date('YmdHis') ?>">
  <title>Login Movi</title>
</head>

<body>
  <div class="Contentlogin">
    <div class="Contentlogin__int">
      <div class="Contentlogin__form">
        <div class="mb-3">
          <h4>Log in to your account</h4>
        </div>
        <form id="Form-login" class="Forms">
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Email address:</label>
            <input type="email" placeholder="Enter your email address" class="form-control" name="login[email]" required>
            <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Password:</label>
            <input type="password" placeholder="Enter your Password" class="form-control" name="login[password]" required>
          </div>
          <button type="submit" class="btn btn-primary d-block W-full">
            <div class="spinner-border spinner-border-sm load-form" role="status" style="display: none;">
              <span class="visually-hidden">Loading...</span>
            </div>
            Login
          </button>
        </form>
      </div>
    </div>
  </div>
  <script src="assets/js/jquery-3.7.1.min.js"></script>
  <script src="assets/js/script_login.js"></script>
</body>

</html>
