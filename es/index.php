<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Movi Communications</title>
  <?php include("../dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="../dist/css/swiper-bundle.min.css" />
  <style>
    .Top-int-izq {
      width: 40%;
    }

    .Top-int-der {
      width: 60%;
    }

    .swiper-button-next,
    .swiper-button-prev {
      position: relative;
      display: inline-block;
      margin-right: 40px;
      height: 10px;
      outline: none;
    }

    .swiper-button-next:after,
    .swiper-button-prev:after {
      content: '';
    }

    .swiper-pagination {
      width: 77%;
      height: 4px;
      left: 0;
      top: 0;
      position: relative;
      display: inline-block;
    }

    #background-video {
      width: 100%;
      height: 700px;
      object-fit: cover;
    }

    .Conten-superior-btn {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 1;
    }

    @media screen and (max-width: 1024px) {
      #background-video {
        height: 500px;
      }

      .Top-int-der {
        width: 100%;
      }

      .Conten-superior-btn {
        left: 43%;
      }
    }
  </style>
</head>

<body>

  <header>
    <?php include("../dist/libs/secc_include/secc-header-es.php") ?>
  </header>

  <section>
    <div class="Conten-superior">
      <div class="Conten-superior-sombra">
        <div class="Conten-superior-btn">
          <a href="#!" class="Play-video"><img src="dist/assets/images/boton-de-play.svg" class="Btn-play" alt=""></a>
        </div>
        <!-- <video id="background-video" poster="../../dist/assets/images/bag-palmas.jpg" src="dist/assets/images/movicoms_videoc.mp4" playsinline="" loop="" preload></video> -->
      </div>
    </div>
  </section>

  <section id="about">
    <div class="Conten-global Bg-gris">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-blanco Text-center Mora-bor-about Borde-morado-abajo">NOSOTROS</h2>

        <p class="Texto-blanco Parrafo Text-center">En este mundo en el que todo cambia constantemente, nuestro enfoque principal es impactar de forma orgánica a las audiencias clave de nuestros clientes para ayudarlos a lograr sus objetivos.</p>

        <p class="Texto-blanco Parrafo Text-center">En Movi Communications trabajamos para empresas y marcas líderes a nivel mundial. Somos una agencia especializada en crear estrategias de comunicación efectivas que cubran todas las plataformas de medios de difusión, tanto en Estados Unidos como en Latinoamérica. Contamos con oficinas en Los Ángeles, Ciudad de México, Bogotá y Panamá, lo que nos permite mantenernos a la vanguardia en tendencias del mercado global.</p>

        <p class="Texto-blanco Parrafo Text-center">Algunas de nuestras áreas de expertise son: influencer marketing, generación de contenido en medios tradicionales y digitales, creatividad estratégica, brand awareness, relaciones públicas, producción de eventos y estrategias para redes sociales.</p>

      </div>
    </div>
  </section>

  <section id="services">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-negro Text-center">SERVICIOS</h2>
        <p class="Parrafo Text-center">Somos expertos en contar historias que resulten en casos de éxito. Creamos campañas estratégicas que impactan de forma directa, integrando diversas áreas de nuestros servicios dependiendo de las necesidades de nuestros clientes.</p>

      </div>
      <div class="Conten-image-services Services-grande">
        <img src="../dist/assets/images/services4_img.jpg" alt="">
      </div>
      <div class="Conten-image-services Services-movil">
        <img src="../dist/assets/images/services3_img.jpg" alt="">
      </div>
    </div>
  </section>

  <section id="clients">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-negro Text-center">CLIENTES</h2>
        <p class="Parrafo Text-center">Nos esforzamos por satisfacer las necesidades de nuestros clientes, con el fin de ayudarlos a lograr sus objetivos. Estas son algunas de las marcas que nos han brindado su confianza para crear casos de éxito en conjunto.</p>

        <div class="slider-container">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_1.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">WARNER BROS. PICTURES </span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_2.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">FEDERACIÓN MEXICANA DE FÚTBOL</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_3.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">WATTPAD</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_4.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LEAGUES CUP</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_5.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">DESIGUAL</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_6.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">FOX DEPORTES</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_7.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">CASAIDEAS</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_8.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LIVE NATION </span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_9.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">NONGSHIM</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_10.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LIONSGATE+</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img src="dist/assets/images/cliente_slide_13.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">CONVERSE</span>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-buttons">
            <div class="swiper-button-prev">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
              </svg>
            </div>
            <div class="swiper-button-next">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
              </svg>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>

    </div>
  </section>

  <section id="work">
    <div class="Conten-global Bg-gris">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-blanco Text-center Mora-bor-about Borde-morado-abajo">NUESTRO TRABAJO</h2>
        <p class="Texto-blanco Parrafo Text-center">Estamos en una era de constante cambio, por lo que nos mantenemos evolucionando. Nuestra metodología de análisis e investigación, así como nuestro pensamiento estratégico, nos ayudan a identificar oportunidades para crear campañas de comunicación exitosas.</p>

      </div>
  </section>
  <section>
    <!-- Otro caso -->
    <div class="Conten-cas">
      <div class="Conten-casos">
        <div class="Conten-casos-int">
          <a href="case-study/warner-bros">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP1.jpg" alt="">

                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">WARNER BROS</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/wattpad">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP2.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">WATTPAD</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/fox-deportes">
            <div class="Conten-casos-flex Flex-flow">
              <div class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img data-aos="fade-left" data-aos-duration="1500" src="dist/assets/images/CaseP3.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">FOX DEPORTES</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/nongshin">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP44.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">NONGSHIM</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/atandt">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP55.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">AT&T</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/casaideas">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP66.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">CASAIDEAS</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/lionsgate">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP77.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">LIONSGATE+</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Ver más</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>


  <div class="Modalboxcontent Oculto">
    <div class="Modalboxcontent__int">
      <div class="Modalboxcontent__context">
        <div class="Modalboxcontent__context-int">
          <div class="Modalboxcontent__context-top">
            <div class="Modalboxcontent__context-top-left">

            </div>
            <div class="Modalboxcontent__context-top-right">
              <a href="javascript:void(0)" class="Modal-btn-cerrar"><i class="icon-cross"></i></a>
            </div>
          </div>
          <div class="Modalboxcontent__context-middle">
            <div class="Modalboxcontent__iframe">
              <iframe id="Moviframe" frameborder="0" allowfullscreen="" allow="autoplay; fullscreen" src=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include("../dist/libs/secc_include/secc-contacto-es.php") ?>

  <footer>
    <?php include("../dist/libs/secc_include/secc-footer.php") ?>
  </footer>
</body>
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/menu-scroll.js"></script>
<script src="dist/js/menu-public.js?<?php echo time()  ?>"></script>
<script src="dist/js/smooth-scroll.polyfills.js"></script>
<script src="dist/js/funcion-scroll.js"></script>
<script src="dist/js/swiper-bundle.min.js"></script>
<script src="dist/js/aos.js"></script>
<script>
  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 50,
    slidesPerGroup: 1,
    // centeredSlides: true,
    centerInsufficientSlides: true,
    loop: false,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    }
  });

  // $('.Conten-superior-btn').on('click', function() {
  //   $('#background-video').get(0).paused ? $('#background-video').get(0).play() : $('#background-video').get(0).pause();
  //   $('.Conten-superior-btn').hide();
  //   $('.Top-int-der').hide();
  // });

  // $('#background-video').on('click', function() {
  //   $('#background-video').get(0).pause();
  //   $('.Conten-superior-btn').show();
  //   $('.Top-int-der').show();
  // });
</script>

<script>
  $('.Conten-superior-btn').on('click', function() {
    $('#Moviframe').attr('src', 'https://player.vimeo.com/video/543695171?autoplay=1');


    $('.Modalboxcontent').removeClass("Oculto");

  });
  $('.Modal-btn-cerrar').on('click', function() {
    $('.Modalboxcontent').addClass("Oculto");
    $('#Moviframe').attr('src', '');
  });
</script>
<script>
  AOS.init();
  /* $(function() {
    $('#slides').superslides({
      inherit_width_from: '.wide-container',
      inherit_height_from: '.wide-container',
      play: 3000,
      animation: 'fade'
    });
  }); */
</script>

</html>