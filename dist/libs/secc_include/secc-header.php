<div class="Top">
  <div class="Top-int">
    <div class="Top-int-izq">
      <div class="Top-logo">
        <a href="/" aria-label="Movi communications"> <img src="dist/assets/images/logo.svg" alt="" id="logocambio"></a>
      </div>
    </div>
    <div class="Top-int-der">
      <div class="Menu-drop Menu-oculto">
        <a href="javascript:void(0)" role="button" id="Drop">
          <img src="dist/assets/images/menu.svg" alt="">
        </a>
      </div>
      <div class="Top-menu Menu-completo Menu-completo-oculto">
        <nav>
          <ul class="Menu-togg">
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="../#about">ABOUT</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="../#services">SERVICES</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="../#clients">CLIENTS</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="../#work">WORK</a></span></li>
            <li class="Menuhover"><span class="hover hover-1"><a data-scroll href="#contact">CONTACT</a></span></li>
            <li class="Menuhover Cambio-idioma"><span class="Ln-es"><a data-scroll href="#!">ES</a></span></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>