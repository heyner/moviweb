<section id="contact">
  <div class="Contacto">
    <div class="Conten-global Bg-gris">
      <div class="Contacto-int">
        <div class="Contacto-int-sec">
          <div class="Contacto-int-sec-part">
            <div class="Bloque-contacto">
              <div class="Bloque-contacto-izq" style="background-image: url(dist/assets/images/img_losangeles.jpg);"></div>
              <div class="Bloque-contacto-der">
                <ul>
                  <li><span class="Titul-ccon">Los Angeles</span></li>
                  <li><span>7080 Hollywood Blvd Suite 1100</span></li>
                  <li><span>Los Angeles, CA 90028</span></li>
                  <li><span>Phone: (1) 323 917 9104</span></li>
                  <li><span>losangeles@movicoms.com</span></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="Contacto-int-sec-part">
            <div class="Bloque-contacto">
              <div class="Bloque-contacto-izq" style="background-image: url(dist/assets/images/img_panama.jpg);"></div>
              <div class="Bloque-contacto-der">
                <ul>
                  <li><span class="Titul-ccon">Panama</span></li>
                  <li><span>Punta Pacífica, Torre de las Américas, A, Piso 12 Panamá, Panamá</span></li>
                  <li><span>Phone: (507) 8355935</span></li>
                  <li><span>panama@movicoms.com</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="Contacto-int-sec">
          <div class="Contacto-int-sec-part">
            <div class="Bloque-contacto">
              <div class="Bloque-contacto-izq" style="background-image: url(dist/assets/images/img_mexico.jpg);"></div>
              <div class="Bloque-contacto-der">
                <ul>
                  <li><span class="Titul-ccon">Mexico</span></li>
                  <li><span>Blvd. Palmas hills , Villa de las palmas, 52787 Naucalpan de Juarez, México</span></li>
                  <li><span>Phone: (52) 55442518360</span></li>
                  <li><span>mexico@movicoms.com</span></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="Contacto-int-sec-part">
            <div class="Bloque-contacto">
              <div class="Bloque-contacto-izq" style="background-image: url(dist/assets/images/img_bogota.jpg);"></div>
              <div class="Bloque-contacto-der">
                <ul>
                  <li><span class="Titul-ccon">Bogota</span></li>
                  <li><span>Calle 96 # 12 – 65 Oficina 414</span></li>
                  <li><span>Bogota, Colombia</span></li>
                  <li><span>Phone: (571) 6217714</span></li>
                  <li><span>bogota@movicoms.com</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="Conten-titul-contac">
      <div class="Conten-titul-contac-int">
        <div class="Titul-vertical">Contact</div>
      </div>
    </div>
  </div>
</section>