   <div class="Conten-otros-casos">
     <div class="Conten-otros-casos-int">
       <h4 class="Titul-h4 Texto-negro Text-center">Other case studies</h4>
       <div class="slider-container">
         <div class="swiper-container Sw-media-min">
           <div class="swiper-wrapper">
             <?php if ($_REQUEST['case'] != 'warner-bros') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/warner-bros">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP1.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">WARNER BROS</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>
             <?php if ($_REQUEST['case'] != 'wattpad') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/wattpad">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP2.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">WATTPAD</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

             <?php if ($_REQUEST['case'] != 'fox-deportes') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/fox-deportes">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP3.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">FOX DEPORTES</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

             <?php if ($_REQUEST['case'] != 'nongshin') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/nongshin">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP44.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">NONGSHIM</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

             <?php if ($_REQUEST['case'] != 'atandt') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/atandt">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP55.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">AT&T</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

             <?php if ($_REQUEST['case'] != 'casaideas') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/casaideas">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP66.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">CASAIDEAS</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

             <?php if ($_REQUEST['case'] != 'lionsgate') {
              ?>
               <div class="swiper-slide">
                 <div class="Conten-otros-casos-slide-horizontal">
                   <div class="Conten-casos3-int">
                     <a href="../case-study/lionsgate">
                       <div class="Conten-casos3-flex Flex-flow">
                         <div class="Conten-medio-work Conten-medio-imagen">
                           <div class="Conten-image3 Image">
                             <img src="dist/assets/images/CaseP77.jpg" alt="">
                           </div>
                         </div>
                         <div class="Conten-medio-work Text-content3">
                           <div class="Conten-medio-text3 Bg-blanco">
                             <div class="Conten-medio-titul">
                               <h3 class="Titul-h4-work">LIONSGATE+</h3>
                               <div class="Conten-slide-btn">
                                 <div class="Btn-slide-mini">
                                   <span class="Btn-amaslide">Learn More</span>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </a>
                   </div>
                 </div>
               </div>
             <?php } ?>

           </div>
         </div>
         <div class="swiper-buttons">
           <div class="swiper-button-prev">
             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
               <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
             </svg>
           </div>
           <div class="swiper-button-next">
             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
               <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
             </svg>
           </div>
         </div>
         <div class="swiper-pagination"></div>
       </div>
     </div>
   </div>

   <!-- <div class="Conten-otros-casos">
   <div class="Conten-otros-casos-int">
     <h4 class="Titul-h4 Texto-negro Text-center">Other case studies</h4>
     <div class="Conten-global">
       <div class="Conten-global-int">
         <div class="slider-container">
           <div class="swiper-container">
             <div class="swiper-wrapper">
               <?php if ($_REQUEST['case'] != 'warner-bros') {
                ?>
                 <div class="swiper-slide">
                   <div class="Conten-otros-casos-slide-horizontal">
                     <div class="Conten-casos3-int">
                       <a href="../case-study/warner-bros">
                         <div class="Conten-casos3-flex Flex-flow">
                           <div class="Conten-medio-work Conten-medio-imagen">
                             <div class="Conten-image3 Image">
                               <img src="https://dkcnews.com/wp-content/uploads/2020/06/Screenshot-2020-06-22-at-15.45.47-1024x681.png" alt="">
                             </div>
                           </div>
                           <div class="Conten-medio-work Text-content3">
                             <div class="Conten-medio-text3 Bg-blanco">
                               <div class="Conten-medio-titul">
                                 <h3 class="Titul-h4-work">WARNER BROS</h3>
                                 <div class="Conten-slide-btn">
                                   <div class="Btn-slide-mini">
                                     <span class="Btn-amaslide">Learn More</span>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               <?php } ?>
               <?php if ($_REQUEST['case'] != 'outshine') {
                ?>
                 <div class="swiper-slide">
                   <div class="Conten-otros-casos-slide-horizontal">
                     <div class="Conten-casos3-int">
                       <a href="../case-study/outshine">
                         <div class="Conten-casos3-flex Flex-flow">
                           <div class="Conten-medio-work Conten-medio-imagen">
                             <div class="Conten-image3 Image">
                               <img src="dist/assets/images/CaseP2.jpg" alt="">
                             </div>
                           </div>
                           <div class="Conten-medio-work Text-content3">
                             <div class="Conten-medio-text3 Bg-blanco">
                               <div class="Conten-medio-titul">
                                 <h3 class="Titul-h4-work">OUTSHINE</h3>
                                 <div class="Conten-slide-btn">
                                   <div class="Btn-slide-mini">
                                     <span class="Btn-amaslide">Learn More</span>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               <?php } ?>

               <?php if ($_REQUEST['case'] != 'mextour') {
                ?>
                 <div class="swiper-slide">
                   <div class="Conten-otros-casos-slide-horizontal">
                     <div class="Conten-casos3-int">
                       <a href="../case-study/mextour">
                         <div class="Conten-casos3-flex Flex-flow">
                           <div class="Conten-medio-work Conten-medio-imagen">
                             <div class="Conten-image3 Image">
                               <img src="dist/assets/images/CaseP3.jpg" alt="">
                             </div>
                           </div>
                           <div class="Conten-medio-work Text-content3">
                             <div class="Conten-medio-text3 Bg-blanco">
                               <div class="Conten-medio-titul">
                                 <h3 class="Titul-h4-work">MEXTOUR</h3>
                                 <div class="Conten-slide-btn">
                                   <div class="Btn-slide-mini">
                                     <span class="Btn-amaslide">Learn More</span>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               <?php } ?>

               <?php if ($_REQUEST['case'] != 'starzplay') {
                ?>
                 <div class="swiper-slide">
                   <div class="Conten-otros-casos-slide-horizontal">
                     <div class="Conten-casos3-int">
                       <a href="../case-study/starzplay">
                         <div class="Conten-casos3-flex Flex-flow">
                           <div class="Conten-medio-work Conten-medio-imagen">
                             <div class="Conten-image3 Image">
                               <img src="dist/assets/images/CaseP4.jpg" alt="">
                             </div>
                           </div>
                           <div class="Conten-medio-work Text-content3">
                             <div class="Conten-medio-text3 Bg-blanco">
                               <div class="Conten-medio-titul">
                                 <h3 class="Titul-h4-work">STARZPLAY</h3>
                                 <div class="Conten-slide-btn">
                                   <div class="Btn-slide-mini">
                                     <span class="Btn-amaslide">Learn More</span>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               <?php } ?>

               <?php if ($_REQUEST['case'] != 'victorias-secret') {
                ?>
                 <div class="swiper-slide">
                   <div class="Conten-otros-casos-slide-horizontal">
                     <div class="Conten-casos3-int">
                       <a href="../case-study/victorias-secret">
                         <div class="Conten-casos3-flex Flex-flow">
                           <div class="Conten-medio-work Conten-medio-imagen">
                             <div class="Conten-image3 Image">
                               <img src="dist/assets/images/CaseP5.jpg" alt="">
                             </div>
                           </div>
                           <div class="Conten-medio-work Text-content3">
                             <div class="Conten-medio-text3 Bg-blanco">
                               <div class="Conten-medio-titul">
                                 <h3 class="Titul-h4-work">VICTORIA'S SECRET</h3>
                                 <div class="Conten-slide-btn">
                                   <div class="Btn-slide-mini">
                                     <span class="Btn-amaslide">Learn More</span>
                                   </div>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </a>
                     </div>
                   </div>
                 </div>
               <?php } ?>
             </div>
           </div>
           <div class="swiper-buttons">
             <div class="swiper-button-prev">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                 <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
               </svg>
             </div>
             <div class="swiper-button-next">
               <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                 <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
               </svg>
             </div>
           </div>
           <div class="swiper-pagination"></div>
         </div>
       </div>
     </div>
     <div class="Conten-otros-casos-slide">
     </div>
   </div>
 </div>
 -->