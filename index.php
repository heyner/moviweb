<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="In a world where everything is constantly changing, our main focus is to impact our target audience in an organic manner that drives our clients’ main objective.">
  <title>Movi Communications</title>
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Organization",
      "name": "Movi Communications",
      "url": "https://movicoms.com",
      "logo": "https://movicoms.com/dist/assets/images/bag-palmas.jpg",
      "description": "Movi Communications specializes in communication strategies across U.S. Hispanic and Latin American markets, offering influencer marketing, content creation, and brand awareness services.",
      "sameAs": [
        "https://www.linkedin.com/company/movi-communications",
        "https://www.instagram.com/movicoms"
      ],
      "contactPoint": {
        "@type": "ContactPoint",
        "contactType": "Customer Service",
        "availableLanguage": ["English", "Spanish"]
      },
      "address": [{
          "@type": "PostalAddress",
          "addressLocality": "Los Angeles",
          "addressRegion": "CA",
          "addressCountry": "US"
        },
        {
          "@type": "PostalAddress",
          "addressLocality": "Mexico City",
          "addressCountry": "MX"
        },
        {
          "@type": "PostalAddress",
          "addressLocality": "Bogota",
          "addressCountry": "CO"
        },
        {
          "@type": "PostalAddress",
          "addressLocality": "Panama City",
          "addressCountry": "PA"
        }
      ],
      "department": {
        "@type": "Organization",
        "name": "Marketing and PR",
        "description": "Specializes in influencer marketing, content creation, media relations, events, and social media strategies."
      },
      "hasOfferCatalog": {
        "@type": "OfferCatalog",
        "name": "Services",
        "itemListElement": [{
            "@type": "Offer",
            "itemOffered": {
              "@type": "Service",
              "name": "Influencer Campaigns",
              "description": "Creating campaigns with targeted impact for brand engagement."
            }
          },
          {
            "@type": "Offer",
            "itemOffered": {
              "@type": "Service",
              "name": "Media Relations",
              "description": "Connecting brands with the media to build awareness."
            }
          },
          {
            "@type": "Offer",
            "itemOffered": {
              "@type": "Service",
              "name": "Event Strategy",
              "description": "Developing engaging event strategies for targeted audiences."
            }
          }
        ]
      },
      "client": [{
          "@type": "Organization",
          "name": "Warner Bros. Pictures"
        },
        {
          "@type": "Organization",
          "name": "Federación Mexicana de Fútbol"
        },
        {
          "@type": "Organization",
          "name": "Wattpad"
        },
        {
          "@type": "Organization",
          "name": "Fox Deportes"
        },
        {
          "@type": "Organization",
          "name": "Desigual"
        },
        {
          "@type": "Organization",
          "name": "Casaideas"
        },
        {
          "@type": "Organization",
          "name": "Live Nation"
        },
        {
          "@type": "Organization",
          "name": "Lionsgate+"
        }
      ]
    }
  </script>

  <?php include("dist/libs/cssvariable/css-variables.php") ?>
  <link rel="stylesheet" href="dist/css/swiper-bundle.min.css" />
  <style>
    .swiper-button-next,
    .swiper-button-prev {
      position: relative;
      display: inline-block;
      margin-right: 40px;
      height: 10px;
      outline: none;
    }

    .swiper-button-next:after,
    .swiper-button-prev:after {
      content: '';
    }

    .swiper-pagination {
      width: 77%;
      height: 4px;
      left: 0;
      top: 0;
      position: relative;
      display: inline-block;
    }

    #background-video {
      width: 100%;
      height: 700px;
      object-fit: cover;
    }

    .Conten-superior-btn {
      position: absolute;
      top: 50%;
      left: 50%;
      z-index: 1;
    }

    .Isize {
      width: 310px;
      height: 500px;
    }

    @media screen and (max-width: 1024px) {
      #background-video {
        height: 500px;
      }

      .swiper-container {
        height: 350px !important;
      }

      .Conten-superior-btn {
        left: 43%;
      }
    }
  </style>
</head>

<body>

  <header>
    <?php include("dist/libs/secc_include/secc-header.php") ?>
  </header>

  <section>
    <div class="Conten-superior">
      <div class="Conten-superior-sombra">
        <div class="Conten-superior-btn">
          <a href="#!" class="Play-video" aria-label="See Video Movi Communications"><img src="dist/assets/images/boton-de-play.svg" class="Btn-play" alt=""></a>
        </div>

        <!-- <video id="background-video" poster="../../dist/assets/images/bag-palmas.jpg" src="dist/assets/images/movicoms_videoc.mp4" playsinline="" loop="" preload></video> -->
        <!-- <video id="background-video" poster="../../dist/assets/images/bag-palmas.jpg" playsinline="" loop="" preload>
          <source src="https://movicoms.com/dist/assets/images/movicoms_videoc.mp4" type="video/mp4">
          <source src="https://movicoms.com/dist/assets/images/movicoms_videoc.ogg" type="video/ogg">
          Your browser does not support the video tag.
        </video> -->


      </div>
    </div>
  </section>

  <section id="about">
    <div class="Conten-global Bg-gris">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-blanco Text-center Mora-bor-about Borde-morado-abajo">About US</h2>

        <p class="Texto-blanco Parrafo Text-center">In a world where everything is constantly changing, our main focus is to impact our target audience in an organic manner that drives our clients’ main objective.</p>

        <p class="Texto-blanco Parrafo Text-center">With offices in Los Angeles, Mexico City, Bogota and Panama, Movi Communications is an agency that specializes in creating effective communication strategies across all media platforms within the U.S. Hispanic and Latin American markets.</p>

        <p class="Texto-blanco Parrafo Text-center">Our areas of expertise range from influencer marketing, content creation and brand awareness to media relations, events, and social media strategies.</p>

      </div>
    </div>
  </section>

  <section id="services">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-negro Text-center">Services</h2>
        <p class="Parrafo Text-center">We tell stories that engage! Be it influencer campaigns, media relations, strategy creation or events we tell your story with direct and targeted impact.</p>

      </div>
      <div class="Conten-image-services Services-grande">
        <img src="dist/assets/images/services_img.png" alt="">
      </div>
      <div class="Conten-image-services Services-movil">
        <img src="dist/assets/images/services2_img.jpg" alt="">
      </div>
    </div>
  </section>

  <section id="clients">
    <div class="Conten-global">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-negro Text-center">Our Clients</h2>
        <p class="Parrafo Text-center">We strive to meet our clients’ needs, for them to stand out, reach their goals and thrive. These are some of the brands that have given us their trust to flourish together over the years.</p>

        <div class="slider-container">
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <div class="swiper-slide swiper-h">
                <img aria-label="case study WARNER BROS. PICTURES" src="dist/assets/images/cliente_slide_1.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">WARNER BROS. PICTURES </span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study FEDERACIÓN MEXICANA DE FÚTBOL" src="dist/assets/images/cliente_slide_2.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">FEDERACIÓN MEXICANA DE FÚTBOL</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study WATTPAD" src="dist/assets/images/cliente_slide_3.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">WATTPAD</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study LEAGUES CUP" src="dist/assets/images/cliente_slide_4.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LEAGUES CUP</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study DESIGUAL" src="dist/assets/images/cliente_slide_5.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">DESIGUAL</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study FOX DEPORTES" src="dist/assets/images/cliente_slide_6.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">FOX DEPORTES</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study CASAIDEAS" src="dist/assets/images/cliente_slide_7.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">CASAIDEAS</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study LIVE NATION" src="dist/assets/images/cliente_slide_8.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LIVE NATION </span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study NONGSHIM" src="dist/assets/images/cliente_slide_9.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">NONGSHIM</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study LIONSGATE" src="dist/assets/images/cliente_slide_10.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">LIONSGATE+</span>
                </div>
              </div>
              <div class="swiper-slide swiper-h">
                <img aria-label="case study CONVERSE" src="dist/assets/images/cliente_slide_13.jpg" />
                <div class="Conten-text-slide">
                  <span class="swiper-title">CONVERSE</span>
                </div>
              </div>
            </div>
          </div>
          <div class="swiper-buttons">
            <div class="swiper-button-prev">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
              </svg>
            </div>
            <div class="swiper-button-next">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 16">
                <path d="M35 16a26.08 26.08 0 0 0 1.26-7H0V7h36.27a26.4 26.4 0 0 0-1.26-7 22 22 0 0 0 13 8A22.06 22.06 0 0 0 35 16z" fill="#282625" fill-rule="evenodd"></path>
              </svg>
            </div>
          </div>
          <div class="swiper-pagination"></div>
        </div>
      </div>

    </div>
  </section>

  <section id="work">
    <div class="Conten-global Bg-gris">
      <div class="Conten-global-int">
        <h2 class="Titul-h2 Texto-blanco Text-center Mora-bor-about Borde-morado-abajo">Work</h2>

        <p class="Texto-blanco Parrafo Text-center">In an era of instant captivation, we continually adjust our approach, research, influencer analysis and Pr blueprints ensuring audience boosting campaigns, appealing content and engaging narratives that spearhead our brands' culture into the spotlight.</p>

      </div>
  </section>
  <section>
    <!-- Otro caso -->
    <div class="Conten-cas">
      <div class="Conten-casos">
        <div class="Conten-casos-int">
          <a href="case-study/warner-bros">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP1.jpg" alt="">

                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">WARNER BROS</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/wattpad">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP2.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">WATTPAD</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/fox-deportes">
            <div class="Conten-casos-flex Flex-flow">
              <div class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img data-aos="fade-left" data-aos-duration="1500" src="dist/assets/images/CaseP3.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">FOX DEPORTES</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/nongshin">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP44.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">NONGSHIM</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/atandt">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP55.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">AT&T</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/casaideas">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-right" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP66.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">CASAIDEAS</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>

        <div class="Conten-casos-int">
          <a href="case-study/lionsgate">
            <div class="Conten-casos-flex Flex-flow">
              <div data-aos="fade-left" data-aos-duration="1500" class="Conten-medio-work Conten-medio-imagen">
                <div class="Conten-image Image">
                  <img src="dist/assets/images/CaseP77.jpg" alt="">
                </div>
              </div>
              <div class="Conten-medio-work Text-content">
                <div class="Conten-medio-text Bg-blanco">
                  <div class="Conten-medio-titul">
                    <h3 class="Titul-h3-work">LIONSGATE+</h3>
                    <div class="Conten-slide-btn">
                      <div class="Btn-slide">
                        <span class="Btn-amaslide">Learn More</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section>


  <div class="Modalboxcontent Oculto">
    <div class="Modalboxcontent__int">
      <div class="Modalboxcontent__context">
        <div class="Modalboxcontent__context-int">
          <div class="Modalboxcontent__context-top">
            <div class="Modalboxcontent__context-top-left">

            </div>
            <div class="Modalboxcontent__context-top-right">
              <a href="javascript:void(0)" role="button" class="Modal-btn-cerrar"><i class="icon-cross"></i></a>
            </div>
          </div>
          <div class="Modalboxcontent__context-middle">
            <div class="Modalboxcontent__iframe">
              <iframe id="Moviframe" frameborder="0" allowfullscreen="" allow="autoplay; fullscreen" src=""></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </div>


  <?php include("dist/libs/secc_include/secc-contacto.php") ?>

  <footer>
    <?php include("dist/libs/secc_include/secc-footer.php") ?>
  </footer>
</body>
<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/menu-scroll.js"></script>
<script src="dist/js/menu-public.js?<?php echo time()  ?>"></script>
<script src="dist/js/smooth-scroll.polyfills.js"></script>
<script src="dist/js/funcion-scroll.js"></script>
<script src="dist/js/swiper-bundle.min.js"></script>
<script src="dist/js/aos.js"></script>
<script>
  const swiper = new Swiper('.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 50,
    slidesPerGroup: 1,
    // centeredSlides: true,
    centerInsufficientSlides: true,
    loop: false,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      640: {
        slidesPerView: 3,
        spaceBetween: 20
      }
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    pagination: {
      el: ".swiper-pagination",
      type: "progressbar",
    }
  });

  // $('.Conten-superior-btn').on('click', function() {
  //   $('#background-video').get(0).paused ? $('#background-video').get(0).play() : $('#background-video').get(0).pause();
  //   $('.Conten-superior-btn').hide();
  //   $('.Top-int-der').hide();
  // });

  // $('#background-video').on('click', function() {
  //   $('#background-video').get(0).pause();
  //   $('.Conten-superior-btn').show();
  //   $('.Top-int-der').show();
  // });
</script>
<script>
  $('.Conten-superior-btn').on('click', function() {
    $('#Moviframe').attr('src', 'https://player.vimeo.com/video/543695171?autoplay=1');


    $('.Modalboxcontent').removeClass("Oculto");

  });
  $('.Modal-btn-cerrar').on('click', function() {
    $('.Modalboxcontent').addClass("Oculto");
    $('#Moviframe').attr('src', '');
  });
</script>
<script>
  AOS.init();
</script>

</html>